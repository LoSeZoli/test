#include "Card.h"
#include <iostream>


void Card::printCard()
{
	std::cout << "N: " << m_number << " - Symb: " << m_symbol << " - Shad: " << m_shading << " - Col: " << m_color << std::endl;
}

bool Card::compare(const Card card) const
{
	return (this->m_color == card.m_color&&this->m_number == card.m_number&&this->m_shading == card.m_shading&&this->m_symbol == card.m_symbol);
}

void Card::setCard(const int n, const int sy, const int sh, const int c)
{
	m_number = n;
	for (auto it : symbol) {
		if (it.second == sy) {
			m_symbol = it.first;
		}
	}
	for (auto it : shading) {
		if (it.second == sh) {
			m_shading = it.first;
		}
	}
	for (auto it : color) {
		if (it.second == c) {
			m_color = it.first;
		}
	}
}

CardTypes::CardTypes()
{
	symbol["Diamond"] = 0;
	symbol["Squiggle"] = 1;
	symbol["Oval"] = 2;

	shading["Solid"] = 0;
	shading["Striped"] = 1;
	shading["open"] = 2;

	color["Red"] = 0;
	color["Green"] = 1;
	color["Blue"] = 2;
}
