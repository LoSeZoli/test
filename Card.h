#pragma once
#include <iostream>
#include <map>

class CardTypes {
public:
	std::map<char*, int> symbol;
	std::map<char*, int> shading;
	std::map<char*, int> color;
	CardTypes();
};

class Card : private CardTypes
{
protected:

	int m_number;
	char* m_symbol;
	char* m_shading;
	char* m_color;
public:
	Card() = default;
	void printCard();
	bool compare(const Card card) const;
	void setCard(const int n,const int sy, const int sh, const int c);
	~Card() = default;
};

