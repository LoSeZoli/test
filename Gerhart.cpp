#include "Gerhart.h"

Gerhart::Gerhart(int p) : n_players(p)
{
	m_players = new Player[n_players];
	m_playing = true;
	
	while (m_playing) {
		//            TODO
	}
}

bool Gerhart::F1(std::vector<Card> deck)
{
	for (int i = 0; i < deck.size(); i++) {
		for (int j = 0; j < deck.size(); j++) {
			for (int k = 0; k < deck.size(); k++) {
				if (i != j&&i != k&&j != k)
					if (F2(i, j, k, deck)) {
						deck.erase(deck.begin() + i);
						deck.erase(deck.begin() + j);
						deck.erase(deck.begin() + k);
						return true;
					}
			}
		}
	}
	return false;
}

bool Gerhart::F2(const int i1, const int i2, const int i3, std::vector<Card> deck) const
{
	return (deck.at(i1).compare(deck.at(i2)) && deck.at(i1).compare(deck.at(i3)) && deck.at(i2).compare(deck.at(i3)));
}

Gerhart::~Gerhart()
{
	delete[] m_players;
}
