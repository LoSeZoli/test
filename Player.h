#pragma once
#include "Card.h"
#include <vector>
#include <iostream>
#include <conio.h>
class Player
{
protected:
	std::vector<Card> m_hand;
public:
	Player() = default;
	bool GetChoice();
	~Player() = default;
};

