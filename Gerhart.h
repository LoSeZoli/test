#pragma once
#include "Player.h"
class Gerhart
{
	int n_players;
	Player *m_players;
	bool m_playing;
public:
	Gerhart(int p=0);
	bool F1(std::vector<Card> deck);
	bool F2(const int i1, const int i2, const int i3, std::vector<Card> deck) const;
	~Gerhart();
};

