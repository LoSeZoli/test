#include "Francois.h"
#include <time.h>
#include <stdlib.h> 

Francois::Francois()
{
	int pos = 0;
	for (int i = 0; i < 3; i++) {
		for (int j = 0; j < 3; j++) {
			for (int k = 0; k < 3; k++) {
				for (int l = 0; l < 3; l++) {
					m_deck[pos].setCard(i, j, k, l);
					pos++;
				}
			}
		}
	}
	Shufle();
}

void Francois::Shufle()
{
	Card aux;
	int k = 0;

	srand(time(NULL));

	for (auto card : m_deck) {
		static int auxp;
		auxp = rand() % 81;
		aux = m_deck[auxp];
		m_deck[auxp] = card;
		card = aux;
	}
	/*for (auto card : m_deck) {
		card.printCard();
	}*/
}

Card Francois::GetCard()
{
	if (pos < 80)
		return m_deck[pos++];
	else
		return Card();;
}


Francois::~Francois()
{
}
